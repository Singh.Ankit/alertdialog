package com.example.alertdialogdemo2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    Button myButton;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myButton=findViewById(R.id.button);
        relativeLayout=findViewById(R.id.colorChange);
        myButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final CharSequence[] items={"Red","Green","Blue"};
                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Pick a color");
                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        Toast.makeText(getApplicationContext(),"You Picked "+items[i],Toast.LENGTH_LONG).show();
                        switch (i)
                        {
                            case 0:
                                relativeLayout.setBackgroundColor(Color.RED);
                                break;
                            case 1:
                                relativeLayout.setBackgroundColor(Color.GREEN);
                                break;
                            case 2:
                                relativeLayout.setBackgroundColor(Color.BLUE);
                                break;
                        }

                    }
                });
                AlertDialog dlg=builder.create();
                dlg.show();


            }
        });
    }
}
